name := "MarsRover"

version := "0.1"

scalaVersion := "2.12.4"

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.3" % Test

libraryDependencies += "org.mockito" % "mockito-core" % "2.7.0" % Test