package marsrover

case class Position(x: Int, y: Int, direction: String) {
  def move(stepSize: Int): Position = direction match {
    case "N" => Position(x, y + stepSize, direction)
    case "E" => Position(x + stepSize, y, direction)
    case "S" => Position(x, y - stepSize, direction)
    case "W" => Position(x - stepSize, y, direction)
  }

  def turnLeft: Position = direction match {
    case "N" => Position(x, y, "W")
    case "W" => Position(x, y, "S")
    case "S" => Position(x, y, "E")
    case _ => Position(x, y, "N")
  }

  def turnRight: Position = direction match {
    case "N" => Position(x, y, "E")
    case "E" => Position(x, y, "S")
    case "S" => Position(x, y, "W")
    case _ => Position(x, y, "N")
  }
}
