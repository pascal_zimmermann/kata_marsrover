package marsrover

case class MarsRover(position: Position, blockingObstacle: Option[Obstacle], obstacles: List[Obstacle], grid: Grid) {
  private val OneStepForward = 1
  private val OneStepBackward = -1

  def moveForward: MarsRover = move(OneStepForward)

  def moveBackward: MarsRover = move(OneStepBackward)

  private def move(stepSize: Int) = performActionIfNotBlocked {
    val targetPosition = grid.wrap(position.move(stepSize))
    obstacles.find(_.isAt(targetPosition)) match {
      case None => copy(position = targetPosition)
      case obstacle => copy(blockingObstacle = obstacle)
    }
  }

  def turnLeft: MarsRover = performActionIfNotBlocked(copy(position = position.turnLeft))

  def turnRight: MarsRover = performActionIfNotBlocked(copy(position = position.turnRight))

  private def performActionIfNotBlocked(action: => MarsRover) =
    if (isBlockedByObstacle) this
    else action

  private def isBlockedByObstacle = blockingObstacle.isDefined
}

object MarsRover {
  private val IntegerPattern = "[-]?[0-9]+"
  private val DirectionPattern = "[NESW]{1}"

  def receives(commands: Array[Char], initialPositionAsString: String = "0:0:N", minXY: String = "-100:-100", maxXY: String = "100:100", obstaclesAsString: List[String] = Nil): String = {
    val initialPosition = parsePosition(initialPositionAsString)
    val grid: Grid = parseGrid(minXY, maxXY)
    val MarsRover(Position(x, y, direction), blockedByObstacle, _, _) = executeInOrder(commands, initialPosition, obstaclesAsString map parseObstacle, grid)
    blockedByObstacle match {
      case Some(Obstacle(obstacleX, obstacleY)) => s"$x:$y:${direction}_$obstacleX:$obstacleY"
      case None => s"$x:$y:$direction"
    }
  }

  private def parseGrid(minXY: String, maxXY: String) = {
    val pattern = s"($IntegerPattern):($IntegerPattern)".r
    val pattern(minX, minY) = minXY
    val pattern(maxX, maxY) = maxXY
    Grid(minX.toInt, minY.toInt, maxX.toInt, maxY.toInt)
  }

  private def parseObstacle(obstacleAsString: String) = {
    val pattern = s"($IntegerPattern):($IntegerPattern)".r
    val pattern(x, y) = obstacleAsString
    Obstacle(x.toInt, y.toInt)
  }

  private def parsePosition(positionAsString: String) = {
    val pattern = s"($IntegerPattern):($IntegerPattern):($DirectionPattern)".r
    val pattern(x, y, direction) = positionAsString
    Position(x.toInt, y.toInt, direction)
  }

  private def executeInOrder(commands: Array[Char], initialPosition: Position, obstacles: List[Obstacle], grid: Grid) = {
    commands.foldLeft(MarsRover(initialPosition, blockingObstacle = None, obstacles, grid)) {
      case (marsRover, 'f') => marsRover.moveForward
      case (marsRover, 'b') => marsRover.moveBackward
      case (marsRover, 'l') => marsRover.turnLeft
      case (marsRover, 'r') => marsRover.turnRight
    }
  }
}
