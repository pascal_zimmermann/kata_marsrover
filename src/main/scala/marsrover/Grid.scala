package marsrover

case class Grid(minX: Int, minY: Int, maxX: Int, maxY: Int) {

  def wrap(position: Position): Position = position.copy(x = wrapX(position.x), y = wrapY(position.y))

  private def wrapX(x: Int): Int = wrap(x, minX, maxX)

  private def wrapY(y: Int): Int = wrap(y, minY, maxY)

  private def wrap(v: Int, min: Int, max: Int) =
    if (v < min) max
    else if (v > max) min
    else v
}
