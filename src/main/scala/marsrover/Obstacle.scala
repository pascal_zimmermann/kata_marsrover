package marsrover

case class Obstacle(x: Int, y: Int) {

  def isAt(position: Position): Boolean = x == position.x && y == position.y
}