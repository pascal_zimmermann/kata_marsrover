package marsrover

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{FlatSpec, GivenWhenThen, Matchers}

class MarsRoverTest extends FlatSpec with Matchers with GivenWhenThen with TableDrivenPropertyChecks {

  "Forward" should "move the mars-rover forward" in {
    val testCases = Table(
      ("commands", "expected result"),
      ("", "0:0:N"),
      ("f", "0:1:N"),
      ("ff", "0:2:N")
    )

    forAll(testCases) { (commands, expectedResult) =>
      Given("0:0:N")

      When(commands)
      val result = MarsRover.receives(commands.toCharArray)

      Then(expectedResult)
      result shouldBe expectedResult
    }
  }

  "Backward" should "move the mars-rover backward" in {
    val testCases = Table(
      ("commands", "expected result"),
      ("b", "0:-1:N"),
      ("bb", "0:-2:N")
    )

    forAll(testCases) { (commands, expectedResult) =>
      Given("0:0:N")

      When(commands)
      val result = MarsRover.receives(commands.toCharArray)

      Then(expectedResult)
      result shouldBe expectedResult
    }
  }

  "Left" should "turn the mars-rover left" in {
    val testCases = Table(
      ("commands", "expected result"),
      ("l", "0:0:W"),
      ("ll", "0:0:S"),
      ("lll", "0:0:E"),
      ("llll", "0:0:N")
    )

    forAll(testCases) { (commands, expectedResult) =>
      Given("0:0:N")

      When(commands)
      val result = MarsRover.receives(commands.toCharArray)

      Then(expectedResult)
      result shouldBe expectedResult
    }
  }

  "Right" should "turn the mars-rover right" in {
    val testCases = Table(
      ("commands", "expected result"),
      ("r", "0:0:E"),
      ("rr", "0:0:S"),
      ("rrr", "0:0:W"),
      ("rrrr", "0:0:N")
    )

    forAll(testCases) { (commands, expectedResult) =>
      Given("0:0:N")

      When(commands)
      val result = MarsRover.receives(commands.toCharArray)

      Then(expectedResult)
      result shouldBe expectedResult
    }
  }

  "A combination of turn and move commands" should "turn the mars rover in the correct direction and move him to the correct spot" in {
    val testCases = Table(
      ("commands", "expected result"),
      ("rf", "1:0:E"),
      ("rrf", "0:-1:S"),
      ("lf", "-1:0:W"),
      ("rb", "-1:0:E"),
      ("rrb", "0:1:S"),
      ("rrrb", "1:0:W"),
      ("rfl", "1:0:N"),
      ("rfflb", "2:-1:N")
    )

    forAll(testCases) { (commands, expectedResult) =>
      Given("0:0:N")

      When(commands)
      val result = MarsRover.receives(commands.toCharArray)

      Then(expectedResult)
      result shouldBe expectedResult
    }
  }

  "The mars rover" should "be able to start at any initial position" in {
    val testCases = Table(
      ("commands", "initial position", "expected result"),
      ("f", "1:0:N", "1:1:N"),
      ("f", "0:1:N", "0:2:N"),
      ("f", "0:0:E", "1:0:E"),
      ("f", "0:0:S", "0:-1:S"),
      ("f", "0:0:W", "-1:0:W"),
      ("f", "10:10:S", "10:9:S"),
      ("f", "0:-1:N", "0:0:N"),
      ("f", "-1:0:E", "0:0:E")
    )

    forAll(testCases) { (commands, initialPosition, expectedResult) =>
      Given(initialPosition)

      When(commands)
      val result = MarsRover.receives(commands.toCharArray, initialPosition)

      Then(expectedResult)
      result shouldBe expectedResult
    }
  }
  it should "stop when encountering an obstacle and report it" in {
    val testCases = Table(
      ("commands", "obstacle", "expected result"),
      ("f", "0:1", "0:0:N_0:1"),
      ("ff", "0:2", "0:1:N_0:2"),
      ("b", "0:-1", "0:0:N_0:-1"),
      ("bb", "0:-2", "0:-1:N_0:-2"),
      ("lff", "-2:0", "-1:0:W_-2:0"),
      ("fffrf", "0:2", "0:1:N_0:2"),
      ("flbbrb", "2:1", "1:1:W_2:1"),
      ("flbbrb", "2:0,1:0", "2:1:N_2:0"),
      ("flbbrb", "1:0,2:0", "2:1:N_2:0")
    )

    forAll(testCases) { (commands, obstacles, expectedResult) =>
      Given("0:0:N")
      Given(s"Obstacles at $obstacles")
      val obstaclesAsString = obstacles.split(',').toList

      When(commands)
      val result = MarsRover.receives(commands.toCharArray, obstaclesAsString = obstaclesAsString)

      Then(expectedResult)
      result shouldBe expectedResult
    }
  }
  it should "wrap from one edge of the grid to the other" in {
    val testCases = Table(
      ("commands", "minMax", "expected result"),
      ("b", "0:0,10:10", "0:10:N"),
      ("llf", "0:0,10:10", "0:10:S"),
      ("lf", "0:0,10:5", "10:0:W"),
      ("rb", "0:0,10:5", "10:0:E"),
      ("fffff", "0:0,10:3", "0:1:N"),
      ("rfffff", "0:0,3:10", "1:0:E"),
      ("rfffff", "-2:-3,3:2", "-1:0:E"),
      ("fffff", "-2:-3,3:2", "0:-1:N"),
      ("fffffffffffff", "-2:-1,3:2", "0:1:N"),
      ("ffffrbbb", "-2:-1,3:2", "3:0:E"),
      ("ffffrbbb", "0:0,0:0", "0:0:E")
    )

    forAll(testCases) { (commands, minMax, expectedResult) =>
      Given("0:0:N")
      val min = minMax.split(',')(0)
      val max = minMax.split(',')(1)
      Given(s"A grid ranging from $min to $max")

      When(commands)
      val result = MarsRover.receives(commands.toCharArray, minXY = min, maxXY = max)

      Then(expectedResult)
      result shouldBe expectedResult
    }
  }

}
